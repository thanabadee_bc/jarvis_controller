#include <ros/ros.h>
#include "jarvis_body_hand/System.hpp"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "jarvis_body_hand");
  ros::NodeHandle nodeHandle("~");

  jarvis_body_hand::System System(nodeHandle);

  ros::spin();
  return 0;
}
